from sqlalchemy import create_engine, select
from model import *
from sqlalchemy.orm import Session


class Setup_DB:
    def __init__(self, url):
        self.__engine = create_engine(url)
        Base.metadata.create_all(self.__engine)

    def login(self, login: str, password: str) -> bool:
        with Session(self.__engine) as session:
            check = select(User).where(User.login == login and User.password == password)  # bool
            if not session.scalars(check).first():
                return False
            else:
                return True

    def register(self, login: str, password: str, email: str) -> bool:
        with Session(self.__engine) as session:
            check = select(User).where(User.login == login or User.email_address == email)
            if not session.scalars(check).first():
                new_user = User(login=login, email_address=email, password=password)  # bool
                session.add(new_user)
                session.commit()
                return True
            else:
                return False

        #     query = select(User).where(User.email_address == email_address and User.login == login)
        #     if not session.scalars(query).first():
        # else:
        #     return False
