from sqlalchemy.orm import Mapped, DeclarativeBase, mapped_column
from sqlalchemy import String, ForeignKey


# second_name: Mapped[str] = mapped_column(String(30), unique=True) with None


class Base(DeclarativeBase):
    pass


class User(Base):
    __tablename__ = "account"

    id: Mapped[int] = mapped_column(primary_key=True)

    login: Mapped[str] = mapped_column(String(30), nullable=False, unique=True)
    email_address: Mapped[str] = mapped_column(String(50), nullable=False, unique=True)
    password: Mapped[str] = mapped_column(String(30), nullable=False)


class Word(Base):
    __tablename__ = "words"

    id: Mapped[int] = mapped_column(primary_key=True)

    english_word = mapped_column(String(30), nullable=False, unique=True)


class Lvl(Base):
    __tablename__ = "lvls"

    id: Mapped[int] = mapped_column(primary_key=True)

    word_id: Mapped[int] = mapped_column(ForeignKey("words.id"))
    translation: Mapped[str] = mapped_column(String(30), nullable=False)
    image_link: Mapped[str] = mapped_column(String(128), nullable=False)
    broken_word: Mapped[str] = mapped_column(String(30), nullable=False)


class Resource(Base):
    __tablename__ = "account_resource"

    id: Mapped[int] = mapped_column(primary_key=True)

    account_id: Mapped[int] = mapped_column(ForeignKey("account.id"))
    health: Mapped[int]
    bonus: Mapped[int]
    lvl: Mapped[int]
