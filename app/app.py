from flask import Flask, render_template, request, redirect, url_for
from db_controller import Setup_DB

db = Setup_DB("mysql+mysqldb://root:mysqlroot@sqldb:3306/employees")

app = Flask(__name__)


@app.route('/', methods=["POST", "GET"])
def index():
    a = [{'tea': ['1', '2', '3']}, {'coffee': ['11', '22', '33']}]  # списки value словарь [{'11':'Нет'}]
    if request.method == 'POST':
        a = request.form.getlist('mycheckbox')
        return f'done{a}'
    return render_template('test_jinja.html',name=a)


@app.route('/res')
def res():
    return render_template('test_jinja.hrml')


@app.route('/register', methods=["POST", "GET"])
def register():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        email = request.form.get('email')
        if db.register(login, password, email):
            return redirect(url_for('test'))
        # error password

    return render_template('reg.html')


@app.route('/login', methods=["POST", "GET"])
def login_page():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        if db.login(login, password):
            return redirect(url_for('test'))
        # error password
    return render_template('log.html')


@app.route('/test', methods=["POST", "GET"])
def test():
    return render_template('registration.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
